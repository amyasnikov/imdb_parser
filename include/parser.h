#include <string>
#include <set>
#include <map>
#include <fstream>
#include <deque>

#include <nlohmann/json.hpp>



namespace http_parser_n {
  struct options_t {
    std::string level    = {};
    std::string file_log = {};
    std::string storage_path  = {};
  };

  struct id_t {
    std::string id    = "tt0000001";
    std::string type  = "movie";
    size_t page       = 0;
    size_t limit      = 100;
    bool raw          = false;
    bool need_request = false;

    std::string get_full_id() const {
      return id + (page ? "_p" + std::to_string(page) : "");
    }
  };

  struct fullcredits_t {
    std::string id;
    std::string type;
    std::deque<std::string> directors;
    std::deque<std::string> writers;
    std::deque<std::string> cast;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(fullcredits_t, id, type, directors, writers, cast);
  };

  struct mediaindex_t {
    std::string id;
    std::string type;
    std::deque<std::string> photos;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(mediaindex_t, id, type, photos);
  };

  struct movie_t {
    std::string id;
    std::string type;
    std::string name;
    std::string year;
    std::string rating_value;
    std::string rating_count;

    std::set<std::string> more_like;
    std::string storyline;
    std::set<std::string> genres;

    fullcredits_t fullcredits;
    std::set<std::string> photos;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(movie_t, id, type, name, year, rating_value, rating_count,
        more_like, storyline, genres,
        fullcredits, photos);
  };

  struct search_t {
    std::string id;
    std::string type;
    std::deque<std::string> ids;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(search_t, id, type, ids);
  };

  struct top_t {
    std::string id;
    std::string type;
    std::deque<movie_t> movies;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(top_t, id, type, movies);
  };



  struct tg_app_movie_t {
    std::string name;
    std::set<std::string> photos;
    std::set<std::string> more_like_names;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(tg_app_movie_t, name, photos, more_like_names);

    bool operator<(const tg_app_movie_t& tg_app_movie) const { return name < tg_app_movie.name; }
  };

  struct tg_app_movies_t {
    std::set<tg_app_movie_t> movies;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(tg_app_movies_t, movies);
  };



  struct parser_t {
    struct film_t {
      std::string name;

      std::string year_production;
      std::set<std::string> country;
      std::set<std::string> genre;
      std::string tagline;

      NLOHMANN_DEFINE_TYPE_INTRUSIVE(film_t, name, year_production, country, genre, tagline);
    };

    std::string storage_path;

    void run(options_t& options);

    // public api
    std::string object_get(const id_t& id);

    // private api
    void object_request(const id_t& id, std::string& data);

    bool file_local_load(const id_t& id, std::string& data);
    void file_local_save(const id_t& id, std::string& data);

    void object_remote(const id_t& id, std::string& data);

    std::string html_parse(const id_t& id, const std::string& data);
    std::string html_parse_movie(const id_t& id, const std::string& data);
    std::string html_parse_fullcredits(const id_t& id, const std::string& data);
    std::string html_parse_mediandex(const id_t& id, const std::string& data);
    std::string html_parse_search(const id_t& id, const std::string& data);
    std::string html_parse_top(const id_t& id, const std::string& data);

    std::string request(const std::string& host, const std::string& target) const;
    std::deque<std::string> xpath_extract(const std::string& data, const std::string& xpath);

    static void err_cb(void *ctx, const char *format, ...) { }
  };



  static inline std::string str_from_file(const std::string& path) {
    std::ifstream file(path);
    std::string str((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    return str;
  }

  static inline void str_to_file(const std::string& str, const std::string& path) {
    std::ofstream file(path);
    file << str;
  }

  inline std::string trim(const std::string &s) {
    auto wsfront = std::find_if_not(s.begin(), s.end(), [](int c) { return std::isspace(c); });
    auto wsback = std::find_if_not(s.rbegin(), s.rend(), [](int c) { return std::isspace(c); }).base();
    return (wsback <= wsfront ? std::string() : std::string(wsfront, wsback));
  }
}

