#include "parser.h"

#include <filesystem>

#include <libxml/HTMLparser.h>
#include <libxml/xpath.h>

#include "logger.h"
#define CPPHTTPLIB_OPENSSL_SUPPORT
#define CPPHTTPLIB_ZLIB_SUPPORT
#include "httplib.h"

namespace http_parser_n {
  void parser_t::run(options_t& options) {
    logger_t::init(options.file_log, options.level);
    TRACER;

    storage_path = options.storage_path;

    if (storage_path.empty()) {
      storage_path = std::filesystem::temp_directory_path();
    }

    try {
#if 0
      id_t id;
      id.need_request = true;
      std::deque<std::string> movie_ids = {"tt0133093", "tt1375666", "tt0108778", "tt0903747"};
      for (const auto& movie_id : movie_ids) {
        id.id = movie_id;
        auto object = object_get(id);
        LOGGER(debug, "object: \n{}", nlohmann::json::parse(object).dump(2));
      }
#else
      id_t id;
      id.type = "top";
      id.limit = 500;
      id.need_request = true;
      auto object = object_get(id);
      LOGGER(debug, "object: \n{}", nlohmann::json::parse(object).dump(2));

      {
        tg_app_movies_t tg_app_movies;
        top_t top = nlohmann::json::parse(object);
        for (const auto& movie : top.movies) {
          tg_app_movie_t tg_app_movie;

          tg_app_movie.name = movie.name;
          if (!movie.year.empty()) {
            tg_app_movie.name += " (" + movie.year + ")";
          }

          tg_app_movie.photos = movie.photos;

          for (const auto& movie_id : movie.more_like) {
            auto it = std::find_if(top.movies.begin(), top.movies.end(),
                [&movie_id](const auto& movie) { return movie.id == movie_id; });
            if (it != top.movies.end()) {
              std::string name = it->name;
              if (!it->year.empty()) {
                name += " (" + it->year + ")";
              }
              tg_app_movie.more_like_names.insert(name);
            }
          }

          tg_app_movies.movies.insert(tg_app_movie);
        }

        str_to_file(nlohmann::json(tg_app_movies).dump(2), std::filesystem::path(storage_path) / "tg_app_movies");
      }
#endif
    } catch (const std::exception& e) {
      LOGGER(err, "exception: {}", e.what());
    }
  }

  std::string parser_t::object_get(const id_t& id) {
    TRACER;
    LOGGER(debug, "id: {} / {}", id.type, id.get_full_id());

    std::string data;
    if (id.need_request || !file_local_load(id, data)) {
      object_request(id, data);
      file_local_save(id, data);
    }

    return data;
  }

  void parser_t::object_request(const id_t& id, std::string& data) {
    TRACER;
    LOGGER(debug, "id: {} / {}", id.type, id.get_full_id());

    id_t id_new = id;
    id_new.raw = true;

    if (!file_local_load(id_new, data)) {
      object_remote(id_new, data);
      file_local_save(id_new, data);
    }

    data = html_parse(id, data);
  }

  bool parser_t::file_local_load(const id_t& id, std::string& data) {
    TRACER;
    LOGGER(debug, "id: {} / {}", id.type, id.get_full_id());

    bool ret = false;
    std::string path = std::filesystem::path(storage_path) / (id.raw ? "raw" : "json") / id.type / id.get_full_id();
    if (auto str = str_from_file(path); !str.empty()) {
      data = str;
      ret = true;
    }
    return ret;
  }

  void parser_t::file_local_save(const id_t& id, std::string& data) {
    TRACER;
    LOGGER(debug, "id: {} / {}", id.type, id.get_full_id());

    std::error_code ec;
    std::string path_dir  = std::filesystem::path(storage_path) / (id.raw ? "raw" : "json") / id.type;
    std::string path = std::filesystem::path(path_dir) / id.get_full_id();
    std::filesystem::create_directories(path_dir, ec);
    str_to_file(data, path);
  }

  void parser_t::object_remote(const id_t& id, std::string& data) {
    TRACER;
    std::string target = "/";
    if (id.type == "movie") {
      target = "/title/" + id.id + "/";
    } else if (id.type == "fullcredits") {
      target = "/title/" + id.id + "/fullcredits/";
    } else if (id.type == "mediaindex") {
      target = "/title/" + id.id + "/mediaindex?refine=still_frame&page=" + std::to_string(id.page);
    } else if (id.type == "search") {
      target = "/search/title/?title_type=all&num_votes=100000,&view=simple&sort=num_votes,desc&start=" + std::to_string(id.page);
    } else if (id.type == "top") {
      target = {};
    } else {
      throw std::runtime_error("unknown type: " + id.type);
    }
    data = request("www.imdb.com", target);
  }

  std::string parser_t::html_parse(const id_t& id, const std::string& data) {
    TRACER;
    std::string ret;
    if (id.type == "movie") {
      ret = html_parse_movie(id, data);
    } else if (id.type == "fullcredits") {
      ret = html_parse_fullcredits(id, data);
    } else if (id.type == "mediaindex") {
      ret = html_parse_mediandex(id, data);
    } else if (id.type == "search") {
      ret = html_parse_search(id, data);
    } else if (id.type == "top") {
      ret = html_parse_top(id, data);
    } else {
      throw std::runtime_error("unknown type: " + id.type);
    }
    return ret;
  }

  std::string parser_t::html_parse_movie(const id_t& id, const std::string& data) {
    TRACER;

    movie_t object{};
    object.id   = id.id;
    object.type = id.type;

    struct rule_t {
      std::string xpath;
      std::function<void(const std::deque<std::string>&, movie_t&)> update;
    };

    std::deque<rule_t> rules = {
      {
        R"XPATH(//div[@id="ratingWidget"]/p/strong)XPATH",
        [](const std::deque<std::string>& content, movie_t& object) {
          if (content.size() == 1) {
            object.name = content[0];
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "name");
          }
        }
      },
      {
        R"XPATH(//div[@id="ratingWidget"]/p//text())XPATH",
        [](const std::deque<std::string>& content, movie_t& object) {
          if (content.size() >= 3 && content[2].front() == '(' && content[2].back() == ')') {
            object.year = content[2].substr(1, content[2].size() - 2);
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "year");
          }
        }
      },
      {
        R"XPATH(//span[@itemprop="ratingValue"])XPATH",
        [](const std::deque<std::string>& content, movie_t& object) {
          if (content.size() == 1) {
            object.rating_value = content[0];
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "rating_value");
          }
        }
      },
      {
        R"XPATH(//span[@itemprop="ratingCount"])XPATH",
        [](const std::deque<std::string>& content, movie_t& object) {
          if (content.size() == 1) {
            object.rating_count = content[0];
#if 0
            object.rating_count.erase(std::remove_if(object.rating_count.begin(),
                              object.rating_count.end(),
                              [](unsigned char x){ return !std::isdigit(x); }),
                object.rating_count.end());
#endif
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "rating_count");
          }
        }
      },
      {
        R"XPATH(//div[@class='rec_view']//div[@class='rec_item']/@data-tconst)XPATH",
        [](const std::deque<std::string>& content, movie_t& object) {
          if (!content.empty()) {
            object.more_like = {content.begin(), content.end()};
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "more_like");
          }
        }
      },
      {
        R"XPATH(//div[@id="titleStoryLine"]//h2[contains(text(),'Storyline')]/following-sibling::div[1]/p/span)XPATH",
        [](const std::deque<std::string>& content, movie_t& object) {
          if (content.size() == 1) {
            object.storyline = content[0];
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "storyline");
          }
        }
      },
      {
        R"XPATH(//h4[contains(text(),'Genres:')]/following-sibling::a)XPATH",
        [](const std::deque<std::string>& content, movie_t& object) {
          if (!content.empty()) {
            object.genres = {content.begin(), content.end()};
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "genres");
          }
        }
      },
    };

    for (const auto& rule : rules) {
      auto content = xpath_extract(data, rule.xpath);
      rule.update(content, object);
    }

    {
      id_t id2 = id;
      id2.type = "fullcredits";
      auto fullcredits_str = object_get(id2);
      object.fullcredits = nlohmann::json::parse(fullcredits_str);
    }

    {
      id_t id2 = id;
      id2.type = "mediaindex";
      for (size_t page = 0; page < 1000; ++page) {
        id2.page = page;
        auto mediaindex_str = object_get(id2);
        mediaindex_t mediaindex = nlohmann::json::parse(mediaindex_str);
        object.photos.insert(mediaindex.photos.begin(), mediaindex.photos.end());

        if (mediaindex.photos.empty()) {
          break;
        }
      }
    }

    return nlohmann::json(object).dump(2);
  }

  std::string parser_t::html_parse_fullcredits(const id_t& id, const std::string& data) {
    TRACER;

    fullcredits_t object{};
    object.id   = id.id;
    object.type = id.type;

    struct rule_t {
      std::string xpath;
      std::function<void(const std::deque<std::string>&, fullcredits_t&)> update;
    };

    std::deque<rule_t> rules = {
      {
        R"XPATH(//h4[@name='director']/following-sibling::table[1]//td[@class='name'])XPATH",
        [](const std::deque<std::string>& content, fullcredits_t& object) {
          if (!content.empty()) {
            object.directors = {content.begin(), content.end()};
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "directors");
          }
        }
      },
      {
        R"XPATH(//h4[@name='writer']/following-sibling::table[1]//td[@class='name'])XPATH",
        [](const std::deque<std::string>& content, fullcredits_t& object) {
          if (!content.empty()) {
            object.writers = {content.begin(), content.end()};
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "writers");
          }
        }
      },
      {
        R"XPATH(//h4[@name='cast']/following-sibling::table[1]//td[2])XPATH",
        [](const std::deque<std::string>& content, fullcredits_t& object) {
          if (!content.empty()) {
            object.cast = {content.begin(), content.end()};
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "cast");
          }
        }
      },
    };

    for (const auto& rule : rules) {
      auto content = xpath_extract(data, rule.xpath);
      rule.update(content, object);
    }

    return nlohmann::json(object).dump(2);
  }

  std::string parser_t::html_parse_mediandex(const id_t& id, const std::string& data) {
    TRACER;

    mediaindex_t object{};
    object.id   = id.id;
    object.type = id.type;

    struct rule_t {
      std::string xpath;
      std::function<void(const std::deque<std::string>&, mediaindex_t&)> update;
    };

    std::deque<rule_t> rules = {
      {
        R"XPATH(//div[@class='media_index_thumb_list']//img/@src)XPATH",
        [](const std::deque<std::string>& content, mediaindex_t& object) {
          if (!content.empty()) {
            object.photos = content;
            for (auto& photo : object.photos) {
              photo = photo.substr(0, photo.find('.', photo.rfind('/')));
            }
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "photos");
          }
        }
      },
    };

    for (const auto& rule : rules) {
      auto content = xpath_extract(data, rule.xpath);
      rule.update(content, object);
    }

    return nlohmann::json(object).dump(2);
  }

  std::string parser_t::html_parse_search(const id_t& id, const std::string& data) {
    TRACER;

    search_t object{};
    object.id   = id.id;
    object.type = id.type;

    struct rule_t {
      std::string xpath;
      std::function<void(const std::deque<std::string>&, search_t&)> update;
    };

    std::deque<rule_t> rules = {
      {
        R"XPATH(//div[@class='lister-list']/div/div/a/img/@data-tconst)XPATH",
        [](const std::deque<std::string>& content, search_t& object) {
          if (!content.empty()) {
            object.ids = {content.begin(), content.end()};
          } else {
            LOGGER(warn, "unknown field: {} {}", object.id, "ids");
          }
        }
      },
    };

    for (const auto& rule : rules) {
      auto content = xpath_extract(data, rule.xpath);
      rule.update(content, object);
    }

    return nlohmann::json(object).dump(2);
  }

  std::string parser_t::html_parse_top(const id_t& id, const std::string& /*data*/) {
    TRACER;

    top_t object{};
    object.id   = id.id;
    object.type = id.type;

    {
      while (object.movies.size() < id.limit) {
        id_t id2_search;
        id2_search.type = "search";
        id2_search.need_request = id.need_request;
        id2_search.page = object.movies.size();

        auto search_str = object_get(id2_search);
        search_t search = nlohmann::json::parse(search_str);

        if (search.ids.empty()) {
          break;
        }

        for (const auto& movie_id : search.ids) {
          id_t id_movie = {};
          id_movie.id = movie_id;
          id_movie.need_request = id.need_request;
          auto object_movie_str = object_get(id_movie);
          LOGGER(debug, "object_movie: \n{}", nlohmann::json::parse(object_movie_str).dump(2));

          if (object.movies.size() >= id.limit) {
            break;
          }

          object.movies.push_back(nlohmann::json::parse(object_movie_str));
        }
      }
    }

    return nlohmann::json(object).dump(2);
  }

  std::deque<std::string> parser_t::xpath_extract(const std::string& data, const std::string& xpath) {
    TRACER;
    LOGGER(debug, "xpath: {}", xpath);

    std::deque<std::string>  ret;

    xmlSetGenericErrorFunc((void*) this, (xmlGenericErrorFunc) err_cb);

    auto doc_free = [](xmlDocPtr doc) { xmlFreeDoc(doc); };
    auto xpath_free = [](xmlXPathContextPtr xpath) { xmlXPathFreeContext(xpath); };
    auto obj_free = [](xmlXPathObjectPtr obj) { xmlXPathFreeObject(obj); };


    std::unique_ptr<xmlDoc, decltype(doc_free)> doc(htmlReadMemory(data.c_str(), data.size(), "", "UTF-8", 0), doc_free);
    if (!doc) return {};

    std::unique_ptr<xmlXPathContext, decltype(xpath_free)> xpath_ctx(xmlXPathNewContext(doc.get()), xpath_free);
    if (!xpath_ctx) return {};

    std::unique_ptr<xmlXPathObject, decltype(obj_free)> obj(xmlXPathEvalExpression((const unsigned char *)xpath.c_str(), xpath_ctx.get()), obj_free);
    if (!obj) return {};

    xmlNodeSetPtr nodes = obj->nodesetval;
    size_t size = (nodes) ? nodes->nodeNr : 0;
    LOGGER(debug, "size: {}", size);
    for (size_t i{}; i < size; ++i) {
      auto& node = nodes->nodeTab[i];

      xmlChar* content = xmlNodeGetContent(node);
      std::string str = trim(std::string((char*) content));
      xmlFree(content);

      LOGGER(debug, "content: index: {}: type: {} content: \"{}\"", i, node->type, str);
      ret.push_back(str);
    }

    return ret;
  }

  std::string parser_t::request(const std::string& host, const std::string& target) const {
    TRACER;
    LOGGER(debug, "host: {}", host);
    LOGGER(debug, "target: {}", target);

    if (target.empty()) {
      return {};
    }

#if 1
    httplib::SSLClient cli(host.c_str());
#else
    httplib::Client cli(host.c_str());
#endif

#if 1
    httplib::Headers headers = {
      {"Accept-Language", "ru"},
    };
    cli.set_default_headers(headers);
#endif

    auto res = cli.Get(target.c_str());
    if (!res) {
      LOGGER(debug, "error: {}", (int) res.error());
      return {};
    }

    LOGGER(debug, "status: {}", res->status);
    LOGGER(debug, "body size: {}", res->body.size());
    return res->body;
  }
}
