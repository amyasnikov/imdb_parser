#include <iostream>
#include <boost/program_options.hpp>

#include "parser.h"

using namespace http_parser_n;

bool parse(options_t& options, int argc, char* argv[]) {
  try {
    using namespace boost::program_options;

    options_description desc{"Options"};
    desc.add_options()
      ("help,h",                                    "Help screen")
      ("log",         value(&options.file_log),     "Write verbose output to file. Stdout is used if file is -")
      ("storage",     value(&options.storage_path), "Storage path")
      ("level",       value(&options.level),        "Log levels: \"trace\", \"debug\", \"info\", \"warning\", \"error\", \"critical\" \"off\"")
      ;

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return false;
    }
    notify(vm);

  } catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return false;
  }

  return true;
}



int main(int argc, char* argv[]) {
  options_t options = {};

  if (!parse(options, argc, argv)) {
    return 1;
  }

  parser_t parser;
  parser.run(options);

  return 0;
}

